package P8_6;

/**
 *
 * Created by Eryka on 10/17/2015.
 */
public class Car {

    private final double mpg;
    private double gas_level;

    /*
        Construct a Car object with an efficiency (miles per gal)
     */

    public Car(double eff){
        mpg = eff;
        gas_level = 0;

    }

    /*
        Drive x amount of miles, reducing gallons in the tank
    */

    public void drive(double miles_driven) {
        if (miles_driven >= 0) {
            if (gas_level > 0) {
                double gal_used = miles_driven/mpg;
                if (gas_level < gal_used) {
                    System.out.println("You will run out of gas driving this much! Drive a smaller amount");
                }
                else {
                    gas_level -= gal_used;
                }
            }
            else {
                System.out.println("Tank is empty! Please addGas.");
            }
        }
        else {
            System.out.println("You cannot drive negative miles");
        }

    }

    /*
        Get current level of gas
     */

    public double getGasLevel() {
        return gas_level;
    }

    /*
        Add gas
     */

    public void addGas(double added_gas) {
        if (added_gas > 0) {
            gas_level += added_gas;
        }

    }

}
