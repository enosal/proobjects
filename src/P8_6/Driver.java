package P8_6;

/**
 * Tests Car
 * Created by Eryka on 10/17/2015.
 */
public class Driver {
    public static void main(String[] args) {

        Car erykaElantra = new Car(38);
        System.out.println("Just bought a car! How much gas did they give me? " + erykaElantra.getGasLevel() +  " gallons.");
        System.out.println("Wow, nothing. Cheap! Better fill it up with some gas");
        erykaElantra.addGas(12);
        System.out.printf("Added gas. How much gas is in the tank? %.2f%n", erykaElantra.getGasLevel());

        System.out.println("Going for a drive!");
        erykaElantra.drive(100);
        System.out.printf("How much gas is left? %.2f%n", erykaElantra.getGasLevel());

        System.out.println("Going for a drive!");
        erykaElantra.drive(1000);
        System.out.printf("How much gas is left? %.2f%n", erykaElantra.getGasLevel());

        System.out.println("Going for a drive!");
        erykaElantra.drive(300);
        System.out.printf("How much gas is left? %.2f%n", erykaElantra.getGasLevel());






    }
}
