package P8_15;

import java.util.Scanner;

/**
 * Tests Cone, Cylinder, Sphere classes
 * THIS APPROACH IS OBJECT-ORIENTED
 * Created by Eryka on 10/18/2015.
 */
public class Driver {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter radius: ");

        double radius, height;

        //get radius input
        if (in.hasNextDouble()) {
            radius = in.nextDouble();
            System.out.print("Enter height: ");
            //get height input
            if (in.hasNextDouble()) {
                height = in.nextDouble();

                Sphere sphere = new Sphere(radius);
                Cylinder cylinder = new Cylinder(radius, height);
                Cone cone = new Cone(radius,height);

                System.out.printf("Sphere Volume: %.2f%n", sphere.getVolume());
                System.out.printf("Sphere Surface Area: %.2f%n", sphere.getSA());
                System.out.printf("Cylinder Volume: %.2f%n", cylinder.getVolume());
                System.out.printf("Cylinder Surface Area: %.2f%n", cylinder.getSA());
                System.out.printf("Cone Volume: %.2f%n",  cone.getVolume());
                System.out.printf("Cone Surface Area: %.2f%n", cone.getSA());

            }
            else {
                System.out.println("ERROR: Height is not a double value");
            }
        }
        else {
            System.out.println("ERROR: Radius is not a double value");
        }


    }
}
