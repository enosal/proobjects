package P8_15;

/**
 * Creates a Cone object of a given radius and height
 * Can retrieve Volume and Surface Area of Cone object
 * Created by Eryka on 10/18/2015.
 */
public class Cone {

    private final double r;
    private final double h;

    public Cone(double radius, double height)
    {
        r = radius;
        h = height;
    }

    /*
        Get Volume of cone
     */
    public double getVolume()
    {
        return (1/3.0) * Math.PI * r * r * h;
    }

    /*
        Get Surface Area of cone
     */
    public double getSA()
    {
        return Math.PI * r * (r + Math.sqrt(h*h + r*r));
    }
}
