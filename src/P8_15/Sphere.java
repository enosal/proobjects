package P8_15;

/**
 * Creates a Sphere object of a given radius.
 * Can retrieve Volume and Surface Area of Sphere object
 * Created by Eryka on 10/18/2015.
 */
public class Sphere {

    private final double r;

    public Sphere(double radius)
    {
        r = radius;
    }

    /*
        Get Volume of sphere
     */
    public double getVolume()
    {
        return (4/3.0) * Math.PI * r * r * r;
    }

    /*
        Get Surface Area of sphere
     */
    public double getSA()
    {
        return 4 * Math.PI * r * r;
    }
}
