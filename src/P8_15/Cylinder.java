package P8_15;

/**
 * Creates a Cylinder object of a given radius and height
 * Can retrieve Volume and Surface Area of Cylinder object
 * Created by Eryka on 10/18/2015.
 */
public class Cylinder {

    private final double r;
    private final double h;

    public Cylinder(double radius, double height)
    {
        r = radius;
        h = height;
    }

    /*
        Get Volume of a cylinder
     */
    public double getVolume()
    {
        return Math.PI * r * r * h;
    }

    /*
        Get Surface Area of a cylinder
     */
    public double getSA()
    {
        return (2 * Math.PI * r) * (r + h);
    }
}
