package P8_9;

import java.util.ArrayList;

/**
 * * Simulates a combination lock that has a 3 int combination, left turn, right turn, and open methods
 * The lock is reset after every incorrect number
 * Created by Eryka on 10/18/2015.
 */
public class ComboLock {

    private final int secret1, secret2, secret3;

    private int points_to;
    private String last_turned;
    private int turn_count;

    public ComboLock(int num1, int num2, int num3) {
        secret1 = num1;
        secret2 = num2;
        secret3 = num3;
        this.reset();
    }

    /*
        Resets lock history
     */

    public void reset() {
        points_to = 0;
        last_turned = "None";
        turn_count = 0;
    }

    /*
        Turn lock COUNTERclockwise
     */

    public void turnLeft (int ticks) {
        points_to = (40 - (ticks - points_to)) % 40;
        if (last_turned.equals("Right")) {
            if (points_to == secret2) {
                last_turned = "Left";
                turn_count = 2;
            }
            else{
                this.reset();
            }
        }
    }

    /*
        Turn lock clockwise
     */
    public void turnRight (int ticks) {
        points_to = (points_to + ticks) % 40;
        if (last_turned.equals("None")) {
            if (points_to == secret1) {
                last_turned = "Right";
                turn_count = 1;
            } else {
                this.reset();
            }
        }
        else if (last_turned.equals("Left")){
            if (points_to == secret3) {
                last_turned = "Right";
                turn_count = 3;
            }
            else {
                this.reset();
            }
        }
    }

    /*
        Test to open the lock
        Return true if it opens, false if it doesn't
     */
    public boolean open() {
        if (turn_count == 3) {
            this.reset();
            return true;
        }
        else {
            return false;
        }

    }


}
