package P8_9;

import java.util.InputMismatchException;

/**
 * Tests ComboLock
 * Created by Eryka on 10/18/2015.
 */
public class Driver {
    public static void main(String[] args) {
        //Create a new lock
        ComboLock myLock = new ComboLock(5, 39, 2);


        //Test1. Test the successful case
        System.out.println("Test1. Expected: True");
        myLock.reset();
        myLock.turnRight(5);
        myLock.turnLeft(6);
        myLock.turnRight(3);
        System.out.println("Lock opens: " + myLock.open() + "\n");

        //Test2. Two right turns
        System.out.println("Test2. Expected: False");
        myLock.reset();
        myLock.turnRight(2);
        myLock.turnRight(3);
        myLock.turnLeft(6);
        myLock.turnRight(3);
        System.out.println("Lock opens: " + myLock.open() + "\n");

        //Test3. Incorrect combo
        System.out.println("Test3. Expected: False");
        myLock.reset();
        myLock.turnRight(10);
        myLock.turnLeft(20);
        myLock.turnRight(1);
        System.out.println("Lock opens: " + myLock.open() + "\n");

        //Test4. Open after 2 turns. Then open after 3rd turn
        System.out.println("Test4. Expected: False then True");
        myLock.reset();
        myLock.turnRight(5);
        myLock.turnLeft(6);
        System.out.println("Lock opens: " + myLock.open());
        myLock.turnRight(3);
        System.out.println("Lock opens: " + myLock.open() + "\n");



    }

}
