package P8_11;

import java.util.ArrayList;

/**
 *  Creates a simple letter
 * Created by Eryka on 10/18/2015.
 */
public class Letter {
    private String getter;
    private String sender;
    private String body;

    /*
        Set-up constructor
     */
    public Letter(String from, String to){
        sender = from;
        getter = to;
        body = "";
    }

    /*
        Adds a line to the body of the text;
     */
    public void addLine(String line) {
        body = body + line + "\n";
    }

    /*
        Returns body
     */
    public String getText() {
        return body;
    }

    /*
        Prints the letter in the required format
     */
    public void main() {
        System.out.println("Dear " + getter +":");
        System.out.println();
        System.out.println(this.getText());
        System.out.println("Sincerely,");
        System.out.println();
        System.out.println(sender);
    }

}
