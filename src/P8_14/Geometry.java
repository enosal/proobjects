package P8_14;

/**
 * Contains static methods for computing volume and surface area for each of a sphere, cone, cylinder
 * Created by Eryka on 10/18/2015.
 */
public class Geometry {
    //Volume of a sphere
    public static double sphereVolume(double r) {
        return (4/3.0) * Math.PI * r * r * r;
    }

    //Surface Area of a sphere
    public static double sphereSurface(double r) {
        return 4 * Math.PI * r * r;
    }

    //Volume of a cylinder
    public static double cylinderVolume(double r, double h) {
        return Math.PI * r * r * h;
    }

    //Surface area of a cylinder
    public static double cylinderSurface(double r, double h) {
        return (2 * Math.PI * r) * (r + h);
    }

    //Volume of a cone
    public static double coneVolume(double r, double h) {
        return (1/3.0) * Math.PI * r * r * h;
    }

    //Surface area of a cone
    public static double coneSurface(double r, double h) {
        return Math.PI * r * (r + Math.sqrt(h*h + r*r));
    }
}
