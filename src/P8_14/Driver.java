package P8_14;

import java.util.Scanner;

/**
 * Tests Geometry static methods
 * Created by Eryka on 10/18/2015.
 */
public class Driver {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter radius: ");

        double radius, height;

        //get radius input
        if (in.hasNextDouble()) {
            radius = in.nextDouble();
            System.out.print("Enter height: ");
            //get height input
            if (in.hasNextDouble()) {
                height = in.nextDouble();

                System.out.printf("Sphere Volume: %.2f%n", Geometry.sphereVolume(radius));
                System.out.printf("Sphere Surface Area: %.2f%n", Geometry.sphereSurface(radius));
                System.out.printf("Cylinder Volume: %.2f%n", Geometry.cylinderVolume(radius, height));
                System.out.printf("Cylinder Surface Area: %.2f%n", Geometry.cylinderSurface(radius, height));
                System.out.printf("Cone Volume: %.2f%n",  Geometry.coneVolume(radius, height));
                System.out.printf("Cone Surface Area: %.2f%n", Geometry.coneSurface(radius, height));

            }
            else {
                System.out.println("ERROR: Height is not a double value");
            }
        }
        else {
            System.out.println("ERROR: Radius is not a double value");
        }


    }
}
