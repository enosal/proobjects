package P8_4;

/**
 * Creates an Address object with house no, street, apt num (optional), city, state, postal_num
 * Created by Eryka on 10/16/2015.
 */
public class Address {
    private int house_num;
    private String street;
    private int apt_num; //optional
    private String city;
    private String state;
    private String postal_num;

    /*
        Constructs an Address with NO apartment number given
     */
    public Address(int house_in, String street_in, String city_in, String state_in, String postal_in) {
        house_num = house_in;
        street = street_in;
        city = city_in;
        state = state_in;
        postal_num = postal_in;
    }

    /*
        Constructs an Address WITH an apartment number given
     */
    public Address(int house_in, String street_in, int apt_in, String city_in, String state_in, String postal_in) {
        house_num = house_in;
        street = street_in;
        apt_num = apt_in;
        city = city_in;
        state = state_in;
        postal_num = postal_in;
    }

    /*
        Prints the address of an Address object
     */

    public void printAdd() {
        String apt = (apt_num != 0) ? " Apt " + apt_num : "";
        System.out.println(house_num + " " + street + apt);
        System.out.println(city + ", " + state + " " + postal_num);
    }

    /*
        Tests whether the current Address object comes before another given Address (by zip code)
        If the zip codes are identical, returns false
     */

    public boolean comesBefore(Address other) {
        String opostal_num = other.postal_num;

        int post_num = Integer.parseInt(postal_num);
        int opost_num = Integer.parseInt(opostal_num);


        return post_num < opost_num;
    }

}
