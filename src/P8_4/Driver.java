package P8_4;

/**
 * Test Address class
 * Created by Eryka on 10/16/2015.
 */
public class Driver {
    public static void main(String[] args) {

        //Address (int house_no, String street_name, (optional) int apt_num, String city_name, String state_name, int zip_code);

        Address address1 = new Address(4956, "S. Luna Ave", "Stickney", "IL", "60638");
        address1.printAdd();

        System.out.println("\tcomes before ");

        Address address2 = new Address(5465, "S. Everett Ave", 102, "Chicago", "IL", "60637");
        address2.printAdd();

        System.out.println("\t:");


        //test comesBefore method
        System.out.println(address1.comesBefore(address2));

    }
}
