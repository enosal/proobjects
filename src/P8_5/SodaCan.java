package P8_5;

/**
 * Create a SodaCan object and get the surface area and volume
 * Created by Eryka on 10/17/2015.
 */
public class SodaCan {

    private double radius;
    private double height;

    public SodaCan(double rad, double hei) {
        radius = rad;
        height = hei;
    }

    /*
        Print the given radius and height
     */

    public void printSpecs() {
        System.out.printf("Radius is: %.2f%nHeight is: %.2f%n", radius, height);
    }

    /*
        Get surface area of a cylinder
        Area = 2 * pi * radius * height + 2 * pi * radius^2
     */

    public double getSurfaceArea() {
        return 2.0 * Math.PI * radius * (height + radius);
    }

    /*
        Get volume of a cylinder
        Volume = pi * radius^2 * height
     */
    public double getVolume() {
        return Math.PI * radius * radius * height;
    }

}
