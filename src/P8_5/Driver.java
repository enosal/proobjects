package P8_5;

/**
 * Tests SodaCan
 * Created by Eryka on 10/17/2015.
 */
public class Driver {
    public static void main(String[] args) {
        SodaCan soda1 = new SodaCan(3, 4);
        soda1.printSpecs();
        System.out.printf("Surface area is: %.2f%n", soda1.getSurfaceArea());
        System.out.printf("Volume is: %.2f%n", soda1.getVolume());

        SodaCan soda2 = new SodaCan(2, 12);
        soda2.printSpecs();
        System.out.printf("Surface area is: %.2f%n", soda2.getSurfaceArea());
        System.out.printf("Volume is: %.2f%n", soda2.getVolume());

    }

}
