package P8_8;

/**
 * Tests Student
 * Created by Eryka on 10/17/2015.
 */
public class Driver {
    public static void main(String[] args) {

        Student eryka = new Student("Nosal", "Eryka");
        System.out.println("Name: " + eryka.getName());

        System.out.println("TotalScore: " + eryka.getTotalScore());
        System.out.println("AvgScore: " + eryka.getAverageScore());

        eryka.addQuiz(91);

        System.out.println("TotalScore: " + eryka.getTotalScore());
        System.out.println("AvgScore: " + eryka.getAverageScore());

        eryka.addQuiz(85.2);

        System.out.println("TotalScore: " + eryka.getTotalScore());
        System.out.println("AvgScore: " + eryka.getAverageScore());

        eryka.addGrade("B+");
        System.out.println("GPA: " + eryka.getGPA());

        eryka.addGrade("B");
        System.out.println("GPA: " + eryka.getGPA());

        eryka.addGrade("E");
        System.out.println("GPA: " + eryka.getGPA());

        eryka.addGrade("F");
        System.out.println("GPA: " + eryka.getGPA());


    }
}
