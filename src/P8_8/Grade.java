package P8_8;

/**
 * Grade class can convert a Grade (string) to a grade point value
 * Created by Eryka on 10/17/2015.
 */
public class Grade {

    private String grade;
    private double point;

    public Grade(String grad) {
        grade = grad;
    }

    /*
        Convert a valid letter grade to its point value
        Invalid letters entered return -1
     */

    public double toGPA() {
        switch(this.grade) {
            case "A+":
                point = 4.0;
                break;
            case "A":
                point = 3.7;
                break;
            case "A-":
                point = 3.4;
                break;
            case "B+":
                point = 3.0;
                break;
            case "B":
                point = 2.7;
                break;
            case "B-":
                point = 2.4;
                break;
            case "C+":
                point = 2.0;
                break;
            case "C":
                point = 1.7;
                break;
            case "C-":
                point = 1.4;
                break;
            case "D+":
                point = 1.0;
                break;
            case "D":
                point = .7;
                break;
            case "D-":
                point = .4;
                break;
            case "F":
                point = 0;
                break;
            default:
                point = -1;
        }
        return point;
    }

}
