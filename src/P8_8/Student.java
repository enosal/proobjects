package P8_8;

/**
 * Simulate a Student's scores and GPA
 * Created by Eryka on 10/17/2015.
 */
public class Student {

    private String fname;
    private String lname;
    private int num_quiz;
    private double tot_score;
    private int num_grad;
    private double tot_point;

    /*
        Create a student w/ their personal info, initialize instance variables to zero
     */
    public Student(String lastname, String firstname) {
        fname = firstname;
        lname = lastname;
        num_quiz = 0;
        tot_score = 0;
        num_grad = 0;
        tot_point = 0;
    }

    /*
        Get a student's full name
     */
    public String getName() {
        return fname + " " + lname;
    }

    /*
        Add a student's quiz
     */
    public void addQuiz(double score) {
        num_quiz++;
        tot_score += score;
    }

    /*
        Retrieve a student's total score
     */
    public double getTotalScore() {
        return tot_score;
    }

    /*
        Get a student's average score
     */
    public double getAverageScore() {
        double avg;
        if (num_quiz > 0) {
            avg = tot_score/ num_quiz;
        }
        else {
            avg = 0;
        }
        return avg;
    }

    /*
        Add a grade to a student's info
     */
    public void addGrade(String grade) {
        //Create a new Grade object
        Grade new_grade = new Grade(grade);

        //get the point value for the letter grade
        double point =  new_grade.toGPA();

        //if the point value is a valid value (meaning the letter grade is valid), add to the total points and the number of grade
        if (point >= 0) {
            tot_point += point;
            num_grad++;
        }
        //If a non-grade letter was entered, throw an error
        else {
            System.out.println("ERROR: Improper grade entered");
        }

    }

    /*
        Retrieve a student's GPA
     */
    public double getGPA() {
        return tot_point/ num_grad;
    }


}
