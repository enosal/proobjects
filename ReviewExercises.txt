#########################################################################
# Use this file to answer Review Exercises from the Big Java textbook
#########################################################################

//R8.1 Encapsulation
Encapsulation is to provide a description of a class's methods' behaviors without showing the implementation.
It is useful because users who use a class can do so without having to be aware of the detailed implentation of it (or of it's changes)

//R8.4 Public interface
The public interface of a class is the collection of all the methods of a class and the description fo their methods.
This differs from the implementation in that the public interface is a layperson description but the implementation is the coding to produce the described behaviors

//R8.7 Instance versus static
An instance method is a method implemented on an object and can access the instance variables of the objects on which it acts.
Static methods are not invoked on objects

//R8.8 Mutator and accessor
A mutator is a method that modifies or changes the object on which it is called upon.
An accessor method does nto change the information of the object that it is called upon but instead queries the object for that information

//R8.9 Implicit parameter
The implicit parameter is the object on which a method is applied to. It's typically not written in the method declarations.
Explicit parameters are all other parameters that are declared in the method.

//R8.10 Implicit parameter
An instance method has exactly one implicit parameter and zero, one, or more than one explicit parameters.
A static method has no implicit parameters because a static method is not called upon an object, which is what the implicit parameter would have to pass.

//R8.12 Constructors
A class can have multiple constructors, but it needs to have at least one (whether it's declared or not)
You can have a class with no constructor; the compiler generates a constructor that initializes all instance variables to their default values.
For a class with multiple constructors, the compiler looks at the type of the explicit parameter passed and matches it to the correct constructor.

//R8.16 Instance variables
Private instance variables can only be accessed by methods of its own class and not an other classes.
It is kept private from access from any other classes. Only public instance variables can be accessed from other classes

//R8.19 The this reference
The "this" reference is the implicit parameter passed (meaning it's the object on which the method is called)
It would be used to clarify that the variables used are instance variables (which belong to an object) and not local variables, or used to call an instance method on the same object (on itself).

//R8.20 Zero, null, false, empty String
Zero is an integer indicating a lack of a quantity. It can be compared to other numerical values using "=="
Null refers to no object at all. It can be compared to objects using "=="
false is a boolean type that has only two values - true or false. It can be compared to other boolean types using "=="
An empty string is of a type string. It can be compared to other strings using ".equals"




